[CmdletBinding(SupportsShouldProcess)]
Param(
    [Parameter(Mandatory)]
    [String]$Name,
    [Parameter(Mandatory)]
    [String]$Path,
    [String]$Base = "C:\tools\duplicacy",
    [DateTime]$Start = (Get-Date -Hour 11 -Minute 0 -Second 0),
    [TimeSpan]$RandomDelay = (New-TimeSpan -Seconds 30),
    [Int]$Repeat = 30,
    [Int]$Threads = 4,
    [String]$Logfile,
    [String]$HealthCheckUri
)

# Create Action
$ActionHash = @{
    Execute = "wscript.exe"
    WorkingDirectory = $Path
}

if ($HealthCheckUri) {
    $ActionHash.Argument = """$(Join-Path -Path $Base -ChildPath "invisible.vbs")"" ""$(Join-Path -Path $Base -ChildPath "duplicacy.exe") -healthcheck $($HealthCheckUri) -log backup -stats -threads $($Threads)"""
} else {
    $ActionHash.Argument = """$(Join-Path -Path $Base -ChildPath "invisible.vbs")"" ""$(Join-Path -Path $Base -ChildPath "duplicacy.exe") -log backup -stats -threads $($Threads)"""
}

# Add logfile
if ($Logfile) {
    $ActionHash.Argument -replace " -log ", " -log $($Logfile) "
}

$Action = New-ScheduledTaskAction @ActionHash

# Create Trigger
$TriggerHash = @{
    Daily = $true
    DaysInterval = 1
    At = $Start
    RandomDelay = $RandomDelay
}

$Trigger = New-ScheduledTaskTrigger @TriggerHash

# Create Settings
$Settings = New-ScheduledTaskSettingsSet -Compatibility Win8

# Create principal
$Principal = New-ScheduledTaskPrincipal -UserId $env:USERNAME -LogonType Interactive

$TaskHash = @{
    TaskName = $Name
    Action = $Action
    Trigger = $Trigger
    Settings = $Settings
    Principal = $Principal
}
$Task = Get-ScheduledTask -TaskName $Name -ErrorAction SilentlyContinue
if ($null -eq $Task) {
    $Task = Register-ScheduledTask @TaskHash
} else {
    $Task = Set-ScheduledTask @TaskHash
}

# Set duration and repitition interval
$Task.Triggers.Repetition.Duration = "P1D"
$Task.Triggers.Repetition.Interval = "PT$($Repeat)M"
$Task | Set-ScheduledTask -ErrorAction SilentlyContinue | Out-Null

