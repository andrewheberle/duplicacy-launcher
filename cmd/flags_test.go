package cmd

import (
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func Test_parseFlags(t *testing.T) {
	// enable debugging during tests
	log.SetLevel(log.DebugLevel)

	tests := []struct {
		name      string
		args      []string
		remainder []string
	}{
		{"no args - should equal", []string{}, []string{}},
		{"only flags - should equal", []string{"-d"}, []string{"-d"}},
		{"log flag only", []string{"-log"}, []string{"-log"}},
		{"log flag - no file", []string{"-log", "backup"}, []string{"-log", "backup"}},
		{"log flag - no file and flag", []string{"-log", "-d"}, []string{"-log", "-d"}},
		{"log flag - no file, flag and command", []string{"-log", "-d", "backup"}, []string{"-log", "-d", "backup"}},
		{"log flag - with file", []string{"-logfile", "logfile.log"}, []string{}},
		{"no flags - should equal", []string{"backup", "-stats"}, []string{"backup", "-stats"}},
		{"-d flag - should equal", []string{"-d", "backup", "-stats"}, []string{"-d", "backup", "-stats"}},
		{"-debug flag - should equal", []string{"-debug", "backup", "-stats"}, []string{"-debug", "backup", "-stats"}},
		{"-healthcheck flag - should equal", []string{"-healthcheck", "https://uri", "backup", "-stats"}, []string{"backup", "-stats"}},
		{"-healthcheck flag and -nolock - should equal", []string{"-healthcheck", "https://uri", "-nolock", "backup", "-stats"}, []string{"backup", "-stats"}},
		{"-v flag - should equal", []string{"-v", "backup", "-stats"}, []string{"-v", "backup", "-stats"}},
		{"-? - should equal", []string{"-?"}, []string{"-?"}},
		{"action and -? - should equal", []string{"prune", "-?"}, []string{"prune", "-?"}},
	}
	for _, tt := range tests {
		remainder := parseFlags(tt.args)
		assert.Equal(t, tt.remainder, remainder, tt.name)
	}
}
