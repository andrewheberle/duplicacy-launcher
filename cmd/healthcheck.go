package cmd

import (
	"net/http"
	"net/url"
	"path"
)

func pingHealthCheckUri(action string) {
	if healthCheckUri != "" {
		u, err := url.Parse(healthCheckUri)
		if err != nil {
			return
		}

		u.Path = path.Join(u.Path, action)
		_, _ = http.Get(u.String())
	}
}
