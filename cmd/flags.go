package cmd

import (
	"io"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
)

var healthCheckUri string

func tee(filename string, out *os.File) (w io.Writer) {
	var f *os.File
	var err error
	if f, err = os.Create(filename); err != nil {
		log.WithError(err).Warning("could not create log file")
		return out
	}

	return io.MultiWriter(f, out)
}

func parseFlags(args []string) (parsed []string) {
	// Enable debugging
	if flagExists("-d", args) || flagExists("-debug", args) {
		log.SetLevel(log.DebugLevel)
	}

	// handle healthcheck uri
	healthCheckUri = flagValue("-healthcheck", &args, func(v string) bool { return strings.HasPrefix(v, "http") }, true, true)

	// handle logfile
	if logfile := flagValue("-logfile", &args, func(v string) bool { return strings.HasSuffix(v, ".log") }, true, true); logfile != "" {
		cmdOut = tee(logfile, os.Stdout)
		cmdErr = tee(logfile, os.Stderr)
	}

	// handle explicit no locks
	if flagExists("-nolock", args) {
		nolock = true
		flagRemove("-nolock", &args)
	}

	return args
}

func flagValue(name string, args *[]string, matchfunc func(val string) bool, removeflag, removevalue bool) (value string) {
	var valuenext bool

	if !flagExists(name, *args) {
		return ""
	}

	remainder := make([]string, 0)

	for _, v := range *args {
		if v == name {
			valuenext = true
			if removeflag {
				continue
			}
		}

		if valuenext {
			valuenext = false
			if matchfunc(v) {
				value = v
				if removevalue {
					continue
				}
			}
		}

		remainder = append(remainder, v)
	}
	*args = remainder

	return
}

func flagExists(name string, args []string) bool {
	for _, v := range args {
		if v == name {
			return true
		}
	}
	return false
}

func flagRemove(name string, args *[]string) {
	remainder := make([]string, 0)
	for _, v := range *args {
		if v == name {
			continue
		}
		remainder = append(remainder, v)
	}

	*args = remainder
}
