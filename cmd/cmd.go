package cmd

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strings"

	"github.com/Masterminds/semver/v3"
	"github.com/gofrs/flock"
	log "github.com/sirupsen/logrus"
)

var cmdOut, cmdErr io.Writer
var nolock bool

func executableName(version string) string {
	return duplicacyPrefix + version + duplicacySuffix
}

func getBasePath() (string, error) {
	exe, err := os.Executable()
	if err != nil {
		return "", err
	}

	return filepath.Dir(exe), nil
}

// Run is the main function
func Run() error {
	// start with defaults for Stdout and Stderr
	cmdOut = os.Stdout
	cmdErr = os.Stderr

	// parse command line flags
	args := parseFlags(os.Args[1:])

	// set output for logging
	log.SetOutput(cmdOut)

	if !nolock {
		// create lock file
		lockfile := flock.New(filepath.Join(".", ".duplicacy.lock"))

		// attempt to take lock
		locked, err := lockfile.TryLock()
		if err != nil {
			log.WithError(err).Fatal("error trying to take lock")
		}
		if !locked {
			log.Fatal("another process has a file lock")
		}
		defer func() {
			if err := lockfile.Unlock(); err != nil {
				log.WithError(err).Warning("problem releasing lock")
			}
		}()
	}

	// find where we are running from
	basePath, err := getBasePath()
	if err != nil {
		return fmt.Errorf("error with getBasePath: %w", err)
	}
	log.WithField("basepath", basePath).Debug()

	// find versions in same folder
	matches, err := filepath.Glob(filepath.Join(basePath, executableName("*")))
	if err != nil {
		return fmt.Errorf("error with filepath.Glob: %w", err)
	}

	// start with only an invalid version
	list := []string{"0.0.0"}

	// get versions
	for _, v := range matches {
		ver := strings.Split(strings.TrimSuffix(v, duplicacySuffix), "_")
		if len(ver) == 4 {
			list = append(list, ver[3])
		}
	}

	// convert to slice of semver.Version
	vs := make([]*semver.Version, len(list))
	for i, r := range list {
		v, err := semver.NewVersion(r)
		if err != nil {
			log.WithError(err).WithField("version", r).Debug("version parse error")
			continue
		}

		log.WithField("version", r).Debug("found")

		vs[i] = v
	}

	// sort resulting list of versions
	sort.Sort(semver.Collection(vs))

	// get last entry and check
	duplicacyVersion := vs[len(vs)-1].String()
	if duplicacyVersion == "0.0.0" {
		return fmt.Errorf("did not find valid version")
	}

	log.WithField("version", duplicacyVersion).Debug("version to use")

	// create executable name
	duplicacy := filepath.Join(basePath, executableName(duplicacyVersion))
	log.WithField("duplicacy", duplicacy).Debug("executable name")

	// send start to healthcheckuri
	pingHealthCheckUri("start")

	// output args
	log.WithField("args", args).Debug("command args")

	// create command
	cmd := exec.Command(duplicacy, args...)
	// set up stdin, stdout and stderr
	cmd.Stdin = os.Stdin
	cmd.Stdout = cmdOut
	cmd.Stderr = cmdErr
	// run command
	if err := cmd.Run(); err != nil {
		// send fail to healthcheckuri
		pingHealthCheckUri("fail")
		return fmt.Errorf("Run error: %w", err)
	} else {
		// send ok to healthcheckuri
		pingHealthCheckUri("")
	}

	log.WithField("duplicacy", duplicacy).Debug("command finished")

	return nil
}
