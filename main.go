package main

import (
	cmd "gitlab.com/andrewheberle/duplicacy-launcher/cmd"
	log "github.com/sirupsen/logrus"
)

func main() {
	if err := cmd.Run(); err != nil {
		log.WithError(err).Fatal()
	}
}
