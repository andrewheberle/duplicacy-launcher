# What

This is a launcher to start duplicacy.

## Features

* Will look for duplicacy binaries in the same folder and run the latest version
* Creates a lockfile to ensure actions don't overlap (can be overridden by passing the `-nolock` option)
* Allows HTTP(S) base "ping" via the `-healthcheck <uri>` option (this is stripped before passing to duplicacy)
* Can pass a logfile to `-logfile logfile.log` flag (this is stripped before passing to duplicacy)
* Passes all other args to duplicacy
