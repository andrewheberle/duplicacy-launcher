module gitlab.com/andrewheberle/duplicacy-launcher

go 1.13

require (
	github.com/Masterminds/semver/v3 v3.0.3
	github.com/gofrs/flock v0.7.1
	github.com/kr/pretty v0.2.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.2.2
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
